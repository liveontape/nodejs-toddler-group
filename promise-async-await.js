const freeTables = {
  2: ['one', 'two'],
  3: ['three'],
  4: ['four', 'five', 'six']
}

const foodMenu = {
  'pork': ['meat loaf'],
  'beef': ['goulash'],
  'vegetarian': ['omelett']
}

const drinkMenu = {
  'alcoholic': ['Budwiser', 'Tannenzäpfle'],
  'non-alcoholic': ['Coke', 'Pepsi']
}

exports.eatDinnerInRestaurant = async (setting) => {
  const result = []

  const table = await chooseFreeTable(setting.numberOfPeople)
  if (table) {
    result.push(`Chose table ${table}`)
    console.log(`<me> I chose table ${table}`)

    const promisedTaxi = orderTaxi('Berghain', '10 pm')

    const [ foodSelection, drinkSelection ] =
      await Promise.all([
        chooseFoodFromMenu(setting.mainIngredient),
        chooseDrinkFromMenu(setting.drinkingPreference)
      ])

    result.push(`Ordered ${foodSelection}`)
    console.log(`<me> I chose food ${foodSelection}`)

    result.push(`Ordered ${drinkSelection}`)
    console.log(`<me> I chose drink ${drinkSelection}`)

    const promisedFood = orderFood(foodSelection)
    const promisedDrink = orderDrink(drinkSelection)

    const drink = await promisedDrink
    console.log(`<me> Now having my ${drink}, cheers!`)

    const food = await promisedFood
    console.log(`<me> Great, now having my ${food} on the table!`)
    const price = 50

    await payBill(price)
    console.log(`<me> Payed ${price} bucks for the bill`)
    result.push(`Payed ${price} bucks for the bill`)

    const taxi = await promisedTaxi
    console.log(`<me> Time to go home, taking the taxi (${taxi}) waiting outside!`)
    result.push(`Drank ${drink}`)
    result.push(`Ate ${food}`)
    result.push(`Drove home with a ${taxi}`)
  } else {
    console.log('<me> Frack, no free table; I will cry and go home!')
    result.push('Frack, no free table; I will cry and go home!')
  }
  console.log(`result: ${result}`)
  return result
}

async function chooseFreeTable (seats) {
  return freeTables[seats][0]
}

async function chooseFoodFromMenu (mainIngredient) {
  return foodMenu[mainIngredient][0]
}

async function chooseDrinkFromMenu (drinkingPreference) {
  return drinkMenu[drinkingPreference][0]
}

async function orderFood (foodSelection) {
  console.log(`<Kitchen> Received order, now start cooking ${foodSelection}`)
  await sleep(1000)
  console.log(`<Kitchen> Finished cooking ${foodSelection}`)
  return `fresh ${foodSelection}`
}

async function orderDrink (drinkSelection) {
  console.log(`<Bar> Received order, now start making your drink ${drinkSelection}`)
  await sleep(500)
  console.log(`<Bar> Finished preparing ${drinkSelection} `)
  return `fresh ${drinkSelection}`
}

async function orderTaxi (location, time) {
  console.log(`<Taxi central> Received order, trying to find taxi available to be at ${location} at ${time}`)
  await sleep(500)
  console.log(`<Taxi central> Found taxi, sent it to be at ${location} at ${time}`)
  return 'red Tesla'
}

async function payBill (price) {
  console.log(`<Waiter> ${price} bucks please, sir!`)
  await sleep(500)
  console.log('<Waiter> Thank you, sir!')
}

function sleep (ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}
