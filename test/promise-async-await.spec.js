// /* eslint-env mocha */
const chai = require('chai')
const expect = chai.expect
const index = require('../promise-async-await')

describe('    **** Unit test: index.js ****', async function () {
  it('should test!', async () => {
    // Given

    // When
    const result = await index.eatDinnerInRestaurant({
      numberOfPeople: 2,
      mainIngredient: 'beef',
      drinkingPreference: 'alcoholic'
    })

    // Then
    expect(result).not.to.be.equal(null)
  })
})
